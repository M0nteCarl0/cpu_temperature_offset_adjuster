#pragma once
using namespace System;
using namespace System::Management;
ref class WMI_Motheboard
{
public:
	ManagementObjectSearcher^ WMI_MotheboardS;
	ManagementOperationObserver^ results;
	ManagementObjectCollection^ WMI_MotheboardProperties;
	String^ Manufacture;
	String^ Model;
	String^ Serial;
	String^ Revision;


	WMI_Motheboard(void)
	{
	WMI_MotheboardS = gcnew  ManagementObjectSearcher("root\\CIMV2",
			"SELECT * FROM Win32_BaseBoard");
	WMI_MotheboardProperties = WMI_MotheboardS->Get();
		
		for each (ManagementObject^ queryObj in WMI_MotheboardProperties)
		{
			Manufacture = queryObj["Manufacturer"]->ToString();
			Model = queryObj["Product"]->ToString();
			Serial =queryObj["SerialNumber"]->ToString();
			Revision =  queryObj["Version"]->ToString();
		};


	}
};

