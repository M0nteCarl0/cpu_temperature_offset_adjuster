#pragma once

#include <Windows.h>
#include <OlsApi.h>
#include "CPUInformation.h"
#include "WMI_Motheboard.h"
namespace TJ_TemperatureAdjuster {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	
	/// <summary>
	/// ������ ��� Form1
	/// </summary>
	public ref class MainView : public System::Windows::Forms::Form
	{
	public:


		MainView(void)
		{
		InitializeComponent();
	    bool Flag = 	InitializeOls();
		CPU = new CPUInformation();
		MotherBoard = gcnew WMI_Motheboard();
		DWORD TemperatureTarget;
		DWORD TemperatureTargetOffset;
		DWORD TjActivationTemperture;
		DWORD H, L;





		if (Rdmsr(0x1A2, &L, &H))
		{

			TemperatureTarget = (L >> 16) & 0x7F;
			TemperatureTargetOffset = (L >> 24) & 0x1F;
			TjActivationTemperture = TemperatureTarget + TemperatureTargetOffset;
			TJMax->Text = String::Format ("{0} C",TjActivationTemperture);
			Offset->Text = TemperatureTargetOffset.ToString();
			TJMaxBase->Text  = String::Format("{0} C", TemperatureTarget);
		}

			CPU_Name->Text = gcnew String(CPU->GetBrandName().c_str());
			MotherBoardName->Text = MotherBoard->Manufacture + " " +   MotherBoard->Model;

			if (Rdmsr(0xCE, &L, &H))
			{
				if ((L & 1 << 30) == 0)
				{
					Offset->Enabled = false;
					button1->Enabled = false;
					button3->Enabled = false;
				}

			}
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		CPUInformation*  CPU;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  TJMaxBase;
	protected:
		WMI_Motheboard^ MotherBoard;
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MainView()
		{

			if (CPU)
			{
				delete CPU;
			}
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  Offset;
	protected: 

	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Label^  TJMax;

	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::StatusStrip^  statusStrip1;
	private: System::Windows::Forms::ToolStripStatusLabel^  CPU_Name;
	private: System::Windows::Forms::ToolStripStatusLabel^  MotherBoardName;

	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->Offset = (gcnew System::Windows::Forms::TextBox());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->TJMax = (gcnew System::Windows::Forms::Label());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->statusStrip1 = (gcnew System::Windows::Forms::StatusStrip());
			this->CPU_Name = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->MotherBoardName = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->TJMaxBase = (gcnew System::Windows::Forms::Label());
			this->statusStrip1->SuspendLayout();
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(158, 82);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(98, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"Throtling Thershold";
			// 
			// Offset
			// 
			this->Offset->Location = System::Drawing::Point(262, 79);
			this->Offset->Name = L"Offset";
			this->Offset->Size = System::Drawing::Size(100, 20);
			this->Offset->TabIndex = 1;
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Location = System::Drawing::Point(158, 63);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(42, 13);
			this->label2->TabIndex = 2;
			this->label2->Text = L"TJ Max";
			// 
			// TJMax
			// 
			this->TJMax->AutoSize = true;
			this->TJMax->Location = System::Drawing::Point(259, 63);
			this->TJMax->Name = L"TJMax";
			this->TJMax->Size = System::Drawing::Size(42, 13);
			this->TJMax->TabIndex = 3;
			this->TJMax->Text = L"TJ Max";
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(152, 119);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(75, 23);
			this->button1->TabIndex = 4;
			this->button1->Text = L"Apply";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MainView::button1_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(298, 119);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 5;
			this->button2->Text = L"Read";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &MainView::button2_Click);
			// 
			// statusStrip1
			// 
			this->statusStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) { this->CPU_Name, this->MotherBoardName });
			this->statusStrip1->Location = System::Drawing::Point(0, 233);
			this->statusStrip1->Name = L"statusStrip1";
			this->statusStrip1->Size = System::Drawing::Size(522, 22);
			this->statusStrip1->TabIndex = 6;
			this->statusStrip1->Text = L"statusStrip1";
			// 
			// CPU_Name
			// 
			this->CPU_Name->Name = L"CPU_Name";
			this->CPU_Name->Size = System::Drawing::Size(118, 17);
			this->CPU_Name->Text = L"toolStripStatusLabel1";
			// 
			// MotherBoardName
			// 
			this->MotherBoardName->Name = L"MotherBoardName";
			this->MotherBoardName->Size = System::Drawing::Size(118, 17);
			this->MotherBoardName->Text = L"toolStripStatusLabel2";
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(204, 148);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(108, 23);
			this->button3->TabIndex = 7;
			this->button3->Text = L"Reset to  Default";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &MainView::button3_Click);
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(158, 41);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(69, 13);
			this->label3->TabIndex = 8;
			this->label3->Text = L"TJ Max Base";
			// 
			// TJMaxBase
			// 
			this->TJMaxBase->AutoSize = true;
			this->TJMaxBase->Location = System::Drawing::Point(259, 41);
			this->TJMaxBase->Name = L"TJMaxBase";
			this->TJMaxBase->Size = System::Drawing::Size(69, 13);
			this->TJMaxBase->TabIndex = 9;
			this->TJMaxBase->Text = L"TJ Max Base";
			// 
			// MainView
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(96, 96);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Dpi;
			this->ClientSize = System::Drawing::Size(522, 255);
			this->Controls->Add(this->TJMaxBase);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->statusStrip1);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->TJMax);
			this->Controls->Add(this->label2);
			this->Controls->Add(this->Offset);
			this->Controls->Add(this->label1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedToolWindow;
			this->MaximizeBox = false;
			this->Name = L"MainView";
			this->Text = L"TJ Max Adjuster";
			this->statusStrip1->ResumeLayout(false);
			this->statusStrip1->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {


		DWORD TemperatureTarget;
		DWORD TemperatureTargetOffset;
		DWORD TjActivationTemperture;
		DWORD H, L;
		UINT32 NewOffset = 0;


	
		L = 0;
		H = 0;
		if (Wrmsr(0x1A2, L, H))
		{
			if (Rdmsr(0x1A2, &L, &H))
			{
				TemperatureTarget = (L >> 16) & 0x7F;
				TemperatureTargetOffset = (L >> 24) & 0x1F;
				TjActivationTemperture = TemperatureTarget + TemperatureTargetOffset;
				TJMax->Text = String::Format("{0} C", TjActivationTemperture);
			}
		}

		  DWORD::TryParse(Offset->Text, NewOffset);
		  NewOffset = NewOffset & 0x1F;
		  L|=NewOffset<<24;
		  if (Wrmsr(0x1A2, L, H))
		  {
				if (Rdmsr(0x1A2, &L, &H))
				{
					TemperatureTarget = (L >> 16) & 0x7F;
					TemperatureTargetOffset = (L >> 24) & 0x1F;
					TjActivationTemperture = TemperatureTarget + TemperatureTargetOffset;
					TJMax->Text = String::Format ("{0} C",TjActivationTemperture);
					Offset->Text = TemperatureTargetOffset.ToString();
				}
		 }
		

			 }
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
		
		DWORD TemperatureTarget;
		DWORD TemperatureTargetOffset;
		DWORD TjActivationTemperture;
		DWORD H, L;
		UINT32 NewOffset = 0;

		if (Rdmsr(0x1A2, &L, &H))
		{

			TemperatureTarget = (L >> 16) & 0x7F;
			TemperatureTargetOffset = (L >> 24) & 0x1F;
			TjActivationTemperture = TemperatureTarget + TemperatureTargetOffset;
			TJMax->Text = String::Format ("{0} C",TjActivationTemperture);
			Offset->Text = TemperatureTargetOffset.ToString();
		}

		 }
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {

	DWORD TemperatureTarget;
	DWORD TemperatureTargetOffset;
	DWORD TjActivationTemperture;
	DWORD H, L;
	UINT32 NewOffset = 0;

	H = 0;
	L = 0;
	if (Wrmsr(0x1A2, L, H))
	{
		if (Rdmsr(0x1A2, &L, &H))
		{
			TemperatureTarget = (L >> 16) & 0x7F;
			TemperatureTargetOffset = (L >> 24) & 0x1F;
			TjActivationTemperture = TemperatureTarget + TemperatureTargetOffset;
			TJMax->Text = String::Format("{0} C", TjActivationTemperture);
			Offset->Text = TemperatureTargetOffset.ToString();
		}
	}

}
};
}

